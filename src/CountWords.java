import java.io.*;
import java.util.*;

/**
 * Created by alumniCurie02 on 20/09/2017.
 */
public class CountWords {
    static HashMap dict = new HashMap();
    static long startTime=0;
    public static void main(String[] args) {
        try {
            startTime = System.nanoTime();

            FileReader file = new FileReader(("C:/Users/alumniCurie02/Desktop/aLargeFile"));
//            FileReader file = new FileReader(("C:/Users/alumniCurie02/Desktop/irfan.txt"));
            BufferedReader in = new BufferedReader(file);
            String line;
//possibly store each line in has map then iterate over the keys and ad to hashmap with value equal to line count
            while ((line = in.readLine()) != null) {
//                if (!line.equals("")) {
                    String[] splitted = line.split(" ");
//                    if (splitted.length != 0) {
                        for (int i = 0; i < splitted.length; i++) {
                            String currWord = splitted[i];
                            if (!currWord.equals("")) {
                                if (Character.isAlphabetic(currWord.charAt(0))) {
                                    if (dict.containsKey(currWord)) {
                                        dict.put(currWord, ((int) dict.get(currWord) + 1));
                                    } else {
                                        dict.put(currWord, 1);
                                    }
                                }
                            }
                        }
                    }
//                }
//            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

//
        Object[] arrayOfDict = dict.entrySet().toArray();
        Arrays.sort(arrayOfDict, new Comparator() {
            @Override
            public int compare(Object o1, Object o2) {
                return ((Map.Entry<String, Integer>) o2).getValue().compareTo(((Map.Entry<String, Integer>) o1).getValue());
            }
        });

        for (int j = 0; j < 5; j++) {
            System.out.println(((Map.Entry<String, Integer>) arrayOfDict[j]).getKey() + " : " + ((Map.Entry<String, Integer>) arrayOfDict[j]).getValue());
        }

        final long duration = System.nanoTime() - startTime;
        System.out.println("nano secs: "+duration);
        System.out.println("secs: "+duration/1000000000);

    }
}
